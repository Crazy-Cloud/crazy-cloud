package com.crazy.cloud.user.api.dto;

import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 用户注册dto
 * @author: CrazyMan
 * @create: 2018/9/28 上午9:40
 **/
@Data
public class UserRegisterDTO {

    /**
     * 电话
     */
    private String phoneNo;
    /**
     * 用户名
     */
    private String username;
    /**
     * email
     */
    private String email;
    /**
     * 密码
     */
    private String password;


}
