package com.crazy.cloud.user.api.constant;

/**
 * @program: crazy-cloud
 * @description: UserStatusEnum
 * @author: CrazyMan
 * @create: 2018/10/1 下午9:30
 **/
public enum UserStatusEnum {

    APPROVED("APPROVED"),
    CLOSED("CLOSED"),
    LOGIN_LIMITED("LOGINLIMITED");


    UserStatusEnum(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }
}
