package com.crazy.cloud.user.api.service;

import com.crazy.cloud.common.exception.DataConflictException;
import com.crazy.cloud.user.api.dto.UserDTO;
import com.crazy.cloud.user.api.dto.UserRegisterDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: crazy-cloud
 * @description: 用户Service
 * @author: CrazyMan
 * @create: 2018/9/26 下午2:30
 **/
@FeignClient("user-service")
public interface UserService {

    /**
     * 根据用户id获取用户数据
     *
     * @param clientId 用户唯一id
     */
    @GetMapping("/users/{clientId}")
    UserDTO getUserInfoByClientId(@PathVariable("clientId") String clientId);

    /**
     * 根据电话获取用户数据
     *
     * @param phoneNo 电话
     */
    @GetMapping("/users/phone/{phoneNo}")
    UserDTO getUserInfoByPhoneNo(@PathVariable("phoneNo") String phoneNo);

    /**
     * 根据邮箱获取用户数据
     *
     * @param email 邮箱
     */
    @GetMapping("/users/email")
    UserDTO getUserInfoByEmail(@RequestParam("email") String email);

    /**
     * 更具用户名获取用户数据
     *
     * @param username 用户名称
     */
    @GetMapping("/users/username/{username}")
    UserDTO getUserInfoByUsername(@PathVariable("username") String username);

    /**
     * 注册
     *
     * @param userRegisterDTO 注册信息
     */
    @PostMapping("/users")
    String userRegister(@RequestBody UserRegisterDTO userRegisterDTO) throws DataConflictException;

}
