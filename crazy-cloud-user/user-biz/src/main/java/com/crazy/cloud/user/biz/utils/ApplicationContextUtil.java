package com.crazy.cloud.user.biz.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @program: crazy-cloud
 * @description: 获取上下文
 * @author: CrazyMan
 * @create: 2018/9/28 上午11:07
 **/
@Slf4j
@Component("applicationContextUtil")
@Lazy(false)
public class ApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        log.info("init application success");
        ApplicationContextUtil.applicationContext = applicationContext;
    }

    private static ApplicationContext getApplicationContext() {
        return ApplicationContextUtil.applicationContext;
    }

    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }

    public static void publishEvent(ApplicationEvent event){
        getApplicationContext().publishEvent(event);
    }
}
