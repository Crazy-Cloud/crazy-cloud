package com.crazy.cloud.user.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: crazy-cloud
 * @description: 用户登录DO
 * @author: CrazyMan
 * @create: 2018/9/26 下午4:05
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLogin extends BaseModel {

    private String clientId;

    private String passwordEncryption;

    private String salt;

    private String status;

}
