package com.crazy.cloud.user.dal.dataobject;

import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: crazy-cloud
 * @description: 用户DO
 * @author: CrazyMan
 * @create: 2018/9/25 下午9:11
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class UserDO extends BaseDO {

    /**
     * 用户唯一id
     */
    private String clientId;
    /**
     * email
     */
    private String email;
    /**
     * email激活状态
     */
    private Integer emailActive;
    /**
     * 电话
     */
    private String phoneNo;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 用户注册来源
     */
    private String userOrigin;
    /**
     * 头像
     */
    private String photo;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 性别
     */
    private String sex;
    /**
     * 用户激活状态
     */
    private Integer actived;
    /**
     * 最后登录时间
     */
    private Date lastLoginDate;

}
