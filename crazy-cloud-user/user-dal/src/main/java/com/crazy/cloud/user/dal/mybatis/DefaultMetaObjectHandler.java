package com.crazy.cloud.user.dal.mybatis;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

/**
 * 拦截处理通用字段的填充
 */
public class DefaultMetaObjectHandler extends MetaObjectHandler {


    @Override
    public void insertFill(MetaObject metaObject) {

    }


    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
