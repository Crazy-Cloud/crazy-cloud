package com.crazy.cloud.user.dal.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.crazy.cloud.user.dal.dataobject.UserDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: crazy-cloud
 * @description: 用户mapper
 * @author: CrazyMan
 * @create: 2018/9/25 下午9:10
 **/
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {

}
