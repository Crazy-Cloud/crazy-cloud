# ***********************************************************
# Database: cloud_user
# Generation Time: 2018-09-24 04:33:32 +0000
# ************************************************************


# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` varchar(64) NOT NULL DEFAULT '' COMMENT '系统分配的ClientId',
  `email` varchar(128) NOT NULL DEFAULT '' COMMENT '注册时候Email',
  `emailActive` tinyint(4) NOT NULL DEFAULT '0' COMMENT '邮件是否已激活',
  `phoneNo` varchar(32) NOT NULL DEFAULT '' COMMENT '注册时候的phoneNo',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `nickName` varchar(64) NOT NULL DEFAULT '' COMMENT '昵称',
  `userOrigin` varchar(32) NOT NULL DEFAULT '' COMMENT '用户来源',
  `photo` varchar(128) NOT NULL DEFAULT '' COMMENT '用户头像',
  `birthday` datetime DEFAULT NULL COMMENT '出生年月',
  `sex` varchar(8) NOT NULL DEFAULT '' COMMENT '性别',
  `actived` smallint(6) DEFAULT '0' COMMENT '激活状态',
  `lastLoginDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后登录时间',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新者',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_clientId` (`clientId`),
  UNIQUE KEY `uq_email` (`email`),
  UNIQUE KEY `uq_phoneNo` (`phoneNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

# ------------------------------------------------------------

CREATE TABLE `user_login` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` varchar(64) DEFAULT NULL COMMENT '用户唯一id',
  `passwordEncryption` varchar(512) NOT NULL DEFAULT '' COMMENT '加密密码',
  `salt` varchar(512) NOT NULL DEFAULT '' COMMENT '加密盐',
  `status` varchar(16) NOT NULL DEFAULT '' COMMENT '账户状态',
  `creator` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `modifier` varchar(64) NOT NULL DEFAULT '' COMMENT '更新者',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_clientId` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户登录密码相关表';

# ------------------------------------------------------------

CREATE TABLE `user_event_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` varchar(32) NOT NULL DEFAULT '',
  `loginName` varchar(512) NOT NULL DEFAULT '' COMMENT '用户名',
  `requester` varchar(32) NOT NULL DEFAULT '' COMMENT '请求者; 标识唯一的一个入口',
  `traceId` varchar(64) NOT NULL DEFAULT '' COMMENT '标识一次事件',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT '类型 login / signout',
  `status` varchar(64) NOT NULL DEFAULT '' COMMENT '状态',
  `remark` varchar(1024) NOT NULL DEFAULT '' COMMENT '错误信息',
  `ip` varchar(64) NOT NULL DEFAULT '0.0.0.0' COMMENT '登录IP',
  `browser` varchar(1024) NOT NULL DEFAULT '' COMMENT '使用的浏览器',
  `referer` varchar(1024) NOT NULL DEFAULT '' COMMENT 'referer',
  `userAgent` varchar(512) NOT NULL DEFAULT '',
  `eventTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '事件发生时间',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flag` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_clientId` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户行为信息记录表';