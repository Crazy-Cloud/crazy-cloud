package com.crazy.cloud.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @program: crazy-cloud
 * @description: MD5Util
 * @author: CrazyMan
 * @create: 2018/9/26 下午2:31
 **/
@Slf4j
public class MD5Util {

    private MD5Util() {
    }

    /**
     * 将传入的content进行md5签名
     */
    public static String md5(final String content) {
        if (StringUtils.isBlank(content)) {
            throw new NullPointerException("MD5#md5()传入参数为空");
        }
        return DigestUtils.md5Hex(content);
    }
}
