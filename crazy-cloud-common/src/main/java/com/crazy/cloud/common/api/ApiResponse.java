package com.crazy.cloud.common.api;

/**
 * @program: crazy-cloud
 * @description: 统一返回封装
 * @author: CrazyMan
 * @create: 2018/9/27 下午8:15
 **/
public class ApiResponse<T> {

    private int code = 200;
    private String message;
    private T data;

    private ApiResponse() {
        this.code = 200;
        this.message = "OK";
    }

    private ApiResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiResponse(T data) {
        this.data = data;
    }

    public static <T> ApiResponse<T> success() {
        return new ApiResponse<>();
    }

    public static <T> ApiResponse<T> error(int code, String message) {
        return new ApiResponse<>(code, message);
    }

    public static <T> ApiResponse<T> error(String message) {
        return new ApiResponse<>(500, message);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
