package com.crazy.cloud.common.exception;

/**
 * @program: crazy-cloud
 * @description: 数据重复提交异常
 * @author: CrazyMan
 * @create: 2018/9/25 下午4:35
 **/
public class DataConflictException extends RuntimeException {

    public DataConflictException(String message) {
        super(message);
    }

}
