package com.crazy.cloud.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * @program: crazy-cloud
 * @description: SHAUtil
 * @author: CrazyMan
 * @create: 2018/9/26 下午2:31
 **/
@Slf4j
public class SHAUtil {

    private SHAUtil() {
    }

    public static String sha512Hex(String oldValue) {
        return DigestUtils.sha512Hex(oldValue);
    }


    public static String sha256Hex(String oldValue) {
        return DigestUtils.sha256Hex(oldValue);
    }

}
