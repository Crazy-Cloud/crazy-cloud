package com.crazy.cloud.common.exception;

/**
 * @program: crazy-cloud
 * @description: 服务不可用异常
 * @author: CrazyMan
 * @create: 2018/9/25 下午4:37
 **/
public class UnavailableServiceException extends RuntimeException {

    public UnavailableServiceException() {
    }

    public UnavailableServiceException(String message) {
        super(message);
    }

    public UnavailableServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailableServiceException(Throwable cause) {
        super(cause);
    }

    public UnavailableServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
