package com.crazy.cloud.common.exception;

/**
 * @program: crazy-cloud
 * @description: 数据不存在异常
 * @author: CrazyMan
 * @create: 2018/9/25 下午4:33
 **/
public class DataNotFoundException extends RuntimeException {


    public DataNotFoundException() {

    }

    public DataNotFoundException(String message) {
        super(message);
    }

}
