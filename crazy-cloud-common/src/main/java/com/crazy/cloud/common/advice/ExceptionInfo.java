package com.crazy.cloud.common.advice;

import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 异常信息
 * @author: CrazyMan
 * @create: 2018/9/29 下午2:31
 **/
@Data
public class ExceptionInfo{

    private Long timestamp;

    private Integer status;

    private String exception;

    private String message;

    private String path;

    private String error;


}
