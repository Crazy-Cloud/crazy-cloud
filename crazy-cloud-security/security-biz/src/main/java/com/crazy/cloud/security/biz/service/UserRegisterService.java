package com.crazy.cloud.security.biz.service;

import com.crazy.cloud.security.biz.dto.UserRegisterRequestDTO;

/**
 * @program: crazy-cloud
 * @description: 用户注册service
 * @author: CrazyMan
 * @create: 2018/9/28 下午3:43
 **/
public interface UserRegisterService {

    String register(UserRegisterRequestDTO userRegisterRequestDTO);

}
