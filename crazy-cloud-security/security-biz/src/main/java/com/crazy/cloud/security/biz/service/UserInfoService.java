package com.crazy.cloud.security.biz.service;

import com.crazy.cloud.user.api.dto.UserDTO;

/**
 * @program: crazy-cloud
 * @description: 用户登录service
 * @author: CrazyMan
 * @create: 2018/9/27 下午5:28
 **/
public interface UserInfoService {

    /**
     * 根据clientId查询
     *
     * @param clientId 用户唯一ID
     */
    UserDTO getUserByClient(String clientId);

    /**
     * 根据电话查询
     *
     * @param phoneNo 电话号码
     */
    UserDTO getUserByPhoneNo(String phoneNo);

    /**
     * 根据邮箱查询
     *
     * @param email 邮箱
     */
    UserDTO getUserByEmail(String email);

    /**
     * 用户名查询
     *
     * @param username 用户名
     */
    UserDTO getUserByUsername(String username);

}
