package com.crazy.cloud.security.biz.dto;

import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 用户注册DTO
 * @author: CrazyMan
 * @create: 2018/9/28 上午11:50
 **/
@Data
public class UserRegisterRequestDTO {

    private String username;

    private String phoneNo;

    private String email;

    private String password;

}
