package com.crazy.cloud.security.domain.model;

import java.util.Date;
import lombok.Data;

/**
 * @program: crazy-cloud
 * @description: 基础model
 * @author: CrazyMan
 * @create: 2018/9/26 上午11:20
 **/
@Data
public class BaseModel {

    private String creator;
    private String modifier;
    private Long id;
    private Date created;
    private Date modified;
    private Integer flag;

}
