package com.crazy.cloud.security.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @program: crazy-cloud
 * @description: 自定义异常处理
 * @author: CrazyMan
 * @create: 2018-09-27 13:12
 **/
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
public class CustomOauthException extends OAuth2Exception {
    public CustomOauthException(String msg){
        super(msg);
    }
}
