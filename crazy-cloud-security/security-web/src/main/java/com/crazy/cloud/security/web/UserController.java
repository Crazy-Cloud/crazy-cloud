package com.crazy.cloud.security.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: crazy-cloud
 * @description: 用户信息controller
 * @author: CrazyMan
 * @create: 2018/9/28 上午9:54
 **/
@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {



}
