package com.crazy.cloud.security.config;

import com.crazy.cloud.security.security.CustomAuthenticationEntryPoint;
import com.crazy.cloud.security.security.CustomAuthenticationProvider;
import com.crazy.cloud.security.security.CustomLogoutSuccessHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * @program: crazy-cloud
 * @description: 资源认证EnableResourceServer
 * @author: CrazyMan
 * @create: 2018/9/24 下午3:43
 **/
@Slf4j
@Configuration
@EnableResourceServer
public class OAuth2ResourceConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    CustomLogoutSuccessHandler customLogoutSuccessHandler;
    //token 失效异常返回处理
    @Autowired
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .exceptionHandling()
            .authenticationEntryPoint(customAuthenticationEntryPoint)
            .and()
            .logout()
            .logoutUrl("/oauth/logout")
            .logoutSuccessHandler(customLogoutSuccessHandler)
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS).permitAll()
            .antMatchers(HttpMethod.GET, "/resources/public/**").permitAll()
            .antMatchers(HttpMethod.GET, "/resources/static/**").permitAll()
            .antMatchers("/actuator/**").permitAll()
            .antMatchers("/users/register**").permitAll()
            .anyRequest().authenticated();
    }


}
