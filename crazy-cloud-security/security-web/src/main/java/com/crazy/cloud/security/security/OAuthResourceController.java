package com.crazy.cloud.security.security;

import java.security.Principal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: auth
 * @description: 资源认证
 * @author: CrazyMan
 * @create: 2018-06-06 13:18
 **/
@RestController
public class OAuthResourceController {

    @GetMapping("/oauth/user")
    public Principal user(Principal user) {
        return user;
    }
}
