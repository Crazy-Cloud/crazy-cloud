package com.crazy.cloud.security.config;

import com.crazy.cloud.common.advice.DefaultExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @program: crazy-cloud
 * @description: 异常处理
 * @author: CrazyMan
 * @create: 2018/9/28 下午4:14
 **/
@Slf4j
@RestControllerAdvice
public class ExceptionHandler extends DefaultExceptionHandler {

}
