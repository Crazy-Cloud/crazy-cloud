package com.crazy.cloud.guid.domain.model;

import com.crazy.cloud.common.utils.ConvertUtils;
import com.crazy.cloud.guid.dal.dataobject.GuidDO;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Guid {

    private Long currentId;
    private Long lowerLimit;
    private Long upperLimit;
    private Integer step;

    public static GuidDO convert(Guid guid) {
        return ConvertUtils.convert(guid, GuidDO.class);
    }

    public static Guid convert(GuidDO guidDO) {
        return ConvertUtils.convert(guidDO, Guid.class);
    }

    public static List<Guid> convertData(List<GuidDO> guidDOS) {
        return ConvertUtils.convert(guidDOS, Guid.class);
    }
}
