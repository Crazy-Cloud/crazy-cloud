package com.crazy.cloud.guid.domain.repository;

import com.crazy.cloud.guid.dal.dataobject.GuidDO;
import com.crazy.cloud.guid.dal.mapper.GuidMapper;
import com.crazy.cloud.guid.domain.model.Guid;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class GuidRepository {

    @Autowired
    private GuidMapper guidMapper;

    public int updateNext(String bizType, Long currentIdDb, Long newCurrentId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("bizType", bizType);
        param.put("currentIdDb", currentIdDb);
        param.put("newCurrentId", newCurrentId);
        return guidMapper.updateNext(param);
    }

    public Guid selectNext(String bizType) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("bizType", bizType);
        GuidDO guidDO = guidMapper.selectNext(param);
        return Guid.convert(guidDO);
    }

}
