package com.crazy.cloud.guid.dal.dataobject;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("guid")
public class GuidDO extends BaseDO {

    private String bizType;
    private Long currentId;
    private Long lowerLimit;
    private Long upperLimit;
    private Integer step;
}
