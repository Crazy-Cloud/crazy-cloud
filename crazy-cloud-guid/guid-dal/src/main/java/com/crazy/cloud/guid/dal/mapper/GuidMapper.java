package com.crazy.cloud.guid.dal.mapper;

import com.crazy.cloud.guid.dal.dataobject.GuidDO;
import com.crazy.cloud.guid.dal.mybatis.DefaultMapper;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GuidMapper extends DefaultMapper<GuidDO> {

    int updateNext(Map<String, Object> params);

    GuidDO selectNext(Map<String, Object> params);
}
