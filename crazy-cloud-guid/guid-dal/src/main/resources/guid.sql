

-- ----------------------------
-- Table structure for guid
-- ----------------------------
DROP TABLE IF EXISTS `guid`;
CREATE TABLE `guid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bizType` varchar(64) NOT NULL COMMENT '业务类型',
  `currentId` bigint(20) NOT NULL DEFAULT 0 COMMENT '当前id',
  `lowerLimit` bigint(20) NOT NULL DEFAULT 0 COMMENT '下限',
  `upperLimit` bigint(20) NOT NULL DEFAULT 0 COMMENT '上限',
  `step` int(11) NOT NULL COMMENT '步长',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_bizType` (`bizType`) USING BTREE,
  KEY `idx_bizType_currentId` (`bizType`,`currentId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb64 COMMENT='全局唯一id表';


